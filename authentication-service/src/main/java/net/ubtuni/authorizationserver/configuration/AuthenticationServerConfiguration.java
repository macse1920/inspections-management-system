package net.ubtuni.authorizationserver.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;


@Configuration
@EnableAuthorizationServer
public class AuthenticationServerConfiguration extends AuthorizationServerConfigurerAdapter
{
//	private String ClientID = "R2dpxQ3vPrtfgF72";
//
//	private String ClientSecret = "fDw7Mpkk5czHNuSRtmhGmAGL42CaxQB9";
//
//	private String RedirectURLs = "http://localhost:9002/login/oauth2/code/";

	@Value("${application.oauth.client.id}")
	private String clientId;
	@Value("${application.oauth.client.secret}")
	private String clientSecret;
	@Value("${application.oauth.client.redirect}")
	private String redirectUrl;

	private final PasswordEncoder passwordEncoder;

	public AuthenticationServerConfiguration(PasswordEncoder passwordEncoder)
	{
		this.passwordEncoder = passwordEncoder;
	}

	@Override
	public void configure(AuthorizationServerSecurityConfigurer oauthServer)
	{
		oauthServer.tokenKeyAccess("permitAll()")
				.checkTokenAccess("isAuthenticated()");
	}

	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception
	{
		clients.inMemory()
				.withClient(clientId)
				.secret(passwordEncoder.encode(clientSecret))
				.authorizedGrantTypes("authorization_code")
				.scopes("user_info")
				.autoApprove(true)
				.redirectUris(redirectUrl);
	}

}
