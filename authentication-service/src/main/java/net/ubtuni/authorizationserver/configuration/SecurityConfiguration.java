package net.ubtuni.authorizationserver.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import net.ubtuni.userservice.service.UserAccountService;

//import net.ubtuni.userservice.service.UserAccountService;

@Configuration
@Order(1)
@ComponentScan(basePackages = "net.ubtuni.userservice")
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{
	String[] resources = new String[] {
			"/", "/home", "/pictureCheckCode", "/include/**", "/img/**",
			"/css/**", "/icons/**", "/images/**", "/js/**", "/layer/**",
			"/assets/**", "/base/**", "/icons/**", "/notifications/**",
			"/scss/**", "/vendors/**", "/node_modules/**", "/login**",
			"/vendor/**","/img/**","/css/**","/fonts/**","/js/**","/login",
			"/oauth/authorize","/login.html", "/auth/exit", "/exit"
	};

	@Value("${application.oauth.client.redirect}")
	private String redirectUrl;

	@Autowired
	private UserAccountService userAccountService;

	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.NEVER)
				.and().
				requestMatchers()
				.antMatchers(resources)
				.and()
				.authorizeRequests()
				.antMatchers(resources)
				.permitAll()
				.anyRequest()
				.authenticated()
				.and()
				.formLogin()
				.loginPage("/login")
				.permitAll()
				.and()
				.logout()
				.logoutUrl("/logout")
				.invalidateHttpSession(true)
				.clearAuthentication(true)
				.logoutSuccessUrl("/login")
				.deleteCookies("JSESSIONID", "UISESSION");

	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.userDetailsService(userAccountService);
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}


}
