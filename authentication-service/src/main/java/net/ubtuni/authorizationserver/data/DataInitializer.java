package net.ubtuni.authorizationserver.data;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import net.ubtuni.userservice.data.RolesAndPrivileges;
import net.ubtuni.userservice.models.Person;
import net.ubtuni.userservice.models.Privilege;
import net.ubtuni.userservice.models.Role;
import net.ubtuni.userservice.models.UserAccount;
import net.ubtuni.userservice.repositories.PersonRepository;
import net.ubtuni.userservice.repositories.PrivilegeRepository;
import net.ubtuni.userservice.repositories.RoleRepository;
import net.ubtuni.userservice.repositories.UserAccountRepository;

@Component
public class DataInitializer implements CommandLineRunner
{

	private final UserAccountRepository userAccountRepository;

	private final PrivilegeRepository privilegeRepository;

	private final PersonRepository personRepository;

	private final RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;


	public DataInitializer(final UserAccountRepository userAccountRepository,
			final PrivilegeRepository privilegeRepository, final RoleRepository roleRepository, final PersonRepository personRepository)
	{
		this.userAccountRepository = userAccountRepository;
		this.privilegeRepository = privilegeRepository;
		this.roleRepository = roleRepository;
		this.personRepository = personRepository;
	}


	@Override
	public void run(final String... args)
	{
		setupRoles();
		setupSystemAdministrator();

	}

	private void setupRoles()
	{

		// setup all privileges
		Arrays.stream(RolesAndPrivileges.Privileges
				.values()).forEach(priv -> {

			if (!privilegeRepository.existsByName(priv.name()))
			{
				privilegeRepository.save(new Privilege(priv.name()));
			}

		});


		// create roles and assign privileges
		Arrays.stream(RolesAndPrivileges.values())
				.forEach(rp -> {

					List<Privilege> privileges = privilegeRepository.findByNameIn(Arrays.stream(rp.getPrivileges())
							.map(RolesAndPrivileges.Privileges::name)
							.collect(Collectors.toList()));

					Optional<Role> existingRole = roleRepository.findByName(rp.name());

					if (existingRole.isPresent())
					{
						var roleToBeUpdated = existingRole.orElseThrow();
						roleToBeUpdated.setPrivileges(privileges);

						roleRepository.save(roleToBeUpdated);

					}
					else
					{
						var roleToBeAdded = new Role();
						roleToBeAdded.setName(rp.name());
						roleToBeAdded.setPrivileges(privileges);

						roleRepository.save(roleToBeAdded);
					}


				});

	}

	private void setupSystemAdministrator()
	{
		if (!userAccountRepository.existsByUsername("systemadm"))
		{
			Person person = new Person();
			person.setFirstName("System");
			person.setLastName("Administrator");
			person.setDob(LocalDate.of(1980, 1, 22));

			UserAccount account = new UserAccount();
			account.setUsername("systemadm");
			account.setEmailAddress("systemadm@local.org");
			account.setPassword(passwordEncoder.encode("passwordadm123"));
			account.setRoles(Collections.singleton(roleRepository.findByName(RolesAndPrivileges.ADMIN.name()).orElseThrow()));

			UserAccount savedAccount = userAccountRepository.save(account);

			person.setUserAccount(savedAccount);

			personRepository.save(person);
		}


	}


}
