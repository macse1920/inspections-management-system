package inspectionAssignment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateInspectorAssignment
{
	private long inspectorId;
	private boolean assigned;
	private String zoneId;
}
