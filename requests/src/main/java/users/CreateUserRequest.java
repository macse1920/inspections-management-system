package users;

import java.time.LocalDateTime;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserRequest
{
//	@NotNull

	private String firstName;
	private String lastName;
	private LocalDateTime dateOfBirth;
	private String emailAddress;

	private String username;
	private String role;
}