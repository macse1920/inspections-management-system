package users;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetUsersResponse
{
	private int id;
	private String firstName;
	private String lastName;
	private String username;
	private String role;
}
