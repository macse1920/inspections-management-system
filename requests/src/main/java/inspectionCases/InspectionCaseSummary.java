package inspectionCases;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InspectionCaseSummary
{
	private String id;
	private String title;
	private String status;
	private String reportedBy;
	private String reportedAt;
	private int attachments;
}
