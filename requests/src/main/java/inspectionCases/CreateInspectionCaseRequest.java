package inspectionCases;

import lombok.Data;

@Data
public class CreateInspectionCaseRequest
{
	private String title;
	private String description;
	private String reporterIdentifier;
	private String accusedIdentifier;
	private boolean individual;
	private int street;
}
