package inspectionCases;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DetailedInspectionCase
{
	private String id;
	private String title;
	private String description;

	private String reporterName;
	private String reportedAt;

	private String accusedType;
	private String accusedEntityId;
	private String accusedEntityName;
	private String accusedEntityCity;
}
