package dashboard;

import lombok.Data;

@Data
public class LocationResponse
{
	private String street;
	private String zone;
	private String city;
}
