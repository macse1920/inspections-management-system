package dashboard;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ZonesWithActiveCases
{
	private List<Zone> zones = new ArrayList<>();

	@Data
	public static class Zone
	{
		private String city;
		private String name;
		private int activeCases;
	}

	public void add(Zone zone)
	{
		zones.add(zone);
	}

	@Override
	public String toString()
	{
		return "ZonesWithActiveCases{" +
				"zones=" + Arrays.toString(zones.toArray()) +
				'}';
	}
}
