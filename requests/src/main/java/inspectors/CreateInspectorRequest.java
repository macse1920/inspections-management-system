package inspectors;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class CreateInspectorRequest
{
	private String firstName;
	private String lastName;
	private LocalDateTime dateOfBirth;
	private String emailAddress;
	private String username;
	private String role;


	private long categoryId;
}
