package inspectors;

import lombok.Data;

@Data
public class GetInspectorResponse
{
	private int id;
	private String fullName;
	private String category;
	private String manager;

}
