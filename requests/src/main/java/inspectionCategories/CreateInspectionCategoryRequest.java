package inspectionCategories;

import lombok.Data;

@Data
public class CreateInspectionCategoryRequest
{
	private String name;
	private long managerId;
}
