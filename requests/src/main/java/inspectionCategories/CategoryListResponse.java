package inspectionCategories;

import lombok.Data;

@Data
public class CategoryListResponse
{
	private int id;
	private String name;
	private String ownerFullName;
}
