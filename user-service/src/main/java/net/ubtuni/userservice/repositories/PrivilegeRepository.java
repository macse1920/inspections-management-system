package net.ubtuni.userservice.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.userservice.models.Privilege;

public interface PrivilegeRepository extends JpaRepository<Privilege, Long>
{
	boolean existsByName(String name);

	Optional<Privilege> findByName(String name);

	List<Privilege> findByNameIn(List<String> names);

}
