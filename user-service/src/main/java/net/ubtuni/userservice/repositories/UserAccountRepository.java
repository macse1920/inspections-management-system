package net.ubtuni.userservice.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.userservice.models.UserAccount;

public interface UserAccountRepository extends JpaRepository<UserAccount, Long>
{
	Optional<UserAccount> findByUsername(final String username);

	boolean existsByUsername(String username);
}
