package net.ubtuni.userservice.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.userservice.models.Person;

public interface PersonRepository extends JpaRepository<Person, Long>
{
	Optional<Person> findByUserAccountUsername(final String username);
}
