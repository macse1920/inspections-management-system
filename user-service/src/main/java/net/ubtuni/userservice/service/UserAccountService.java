package net.ubtuni.userservice.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import net.ubtuni.userservice.models.Privilege;
import net.ubtuni.userservice.models.Role;
import net.ubtuni.userservice.models.UserAccount;
import net.ubtuni.userservice.repositories.UserAccountRepository;

@Service
public class UserAccountService implements UserDetailsService
{

	private final UserAccountRepository accountRepository;

	public UserAccountService(final UserAccountRepository accountRepository)
	{
		this.accountRepository = accountRepository;
	}



	@Transactional
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException
	{
		Optional<UserAccount> optionalResult = accountRepository.findByUsername(username);

		UserAccount account = optionalResult.orElseThrow();

//		User us/**/er = new User(account.getUsername(), account.getPassword(), getGrantedAuthorities(account));
		return User.builder()
				.username(account.getUsername())
				.password(account.getPassword())
				.roles(String.valueOf(account.getRoles().stream().map(Role::getName).collect(Collectors.toList())))
				.authorities(getGrantedAuthorities(account))
				.accountExpired(!account.isAccountNonExpired())
				.accountLocked(!account.isAccountNonLocked())
				.disabled(!account.isEnabled())
				.build();

	}

	private List<String> getPrivileges(Collection<Role> roles) {

		List<String> privileges = new ArrayList<>();
		List<Privilege> collection = new ArrayList<>();
		for (Role role : roles) {
			collection.addAll(role.getPrivileges());
		}
		for (Privilege item : collection) {
			privileges.add(item.getName());
		}
		return privileges;
	}

	private Collection<SimpleGrantedAuthority> getGrantedAuthorities(UserAccount userAccount) {
		Collection<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();

		userAccount.getRoles().forEach(role -> role.getPrivileges().forEach(privilege -> simpleGrantedAuthorities.add(new SimpleGrantedAuthority(privilege.getName()))));

		return simpleGrantedAuthorities;
	}
}
