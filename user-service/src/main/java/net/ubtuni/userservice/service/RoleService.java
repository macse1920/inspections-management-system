package net.ubtuni.userservice.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ubtuni.userservice.data.RolesAndPrivileges;
import net.ubtuni.userservice.models.Role;
import net.ubtuni.userservice.repositories.RoleRepository;

@Service
public class RoleService
{
	@Autowired
	private RoleRepository roleRepository;

	public List<String> getAvailableRoles()
	{
		return roleRepository.findAll()
				.stream()
				.filter(r -> !r.getName().equals(RolesAndPrivileges.INSPECTOR.name()))
				.map(Role::getName)
				.collect(Collectors.toList());
	}
}
