package net.ubtuni.userservice.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.stereotype.Service;

import net.ubtuni.userservice.data.RolesAndPrivileges;
import net.ubtuni.userservice.models.CreatedPerson;
import net.ubtuni.userservice.models.Person;
import net.ubtuni.userservice.models.UserAccount;
import net.ubtuni.userservice.repositories.PersonRepository;
import net.ubtuni.userservice.repositories.RoleRepository;
import net.ubtuni.userservice.repositories.UserAccountRepository;
import users.CreateUserRequest;
import users.GetUsersResponse;

@Service
public class UserService
{

	private final PersonRepository personRepository;

	private final UserAccountRepository userAccountRepository;


	@Autowired
	private RoleRepository roleRepository;

	public UserService(final PersonRepository personRepository, final UserAccountRepository userAccountRepository)
	{
		this.personRepository = personRepository;
		this.userAccountRepository = userAccountRepository;
	}

	public CreatedPerson createUser(final CreateUserRequest person, final PasswordEncoder encoder)
	{
		String temporaryPassword = UUID.randomUUID().toString();

		Person p = new Person();
		p.setFirstName(person.getFirstName());
		p.setLastName(person.getLastName());


		UserAccount userAccount = new UserAccount();
		userAccount.setUsername(person.getUsername());
		userAccount.setPassword(encoder.encode(temporaryPassword));
		userAccount.setEmailAddress(person.getEmailAddress());

		var role = roleRepository.findByName(person.getRole()).orElseThrow();
		userAccount.setRoles(Collections.singleton(role));

		UserAccount savedAccount = userAccountRepository.save(userAccount);
		p.setUserAccount(savedAccount);


		return CreatedPerson.builder()
				.person(personRepository.save(p))
				.credential(temporaryPassword)
				.build();
	}

	public List<GetUsersResponse> getUsers(){

		List<GetUsersResponse> result = new ArrayList<>();

		personRepository.findAll().forEach(person -> {

			var getUsersResponse = new GetUsersResponse();
			getUsersResponse.setFirstName(person.getFirstName());
			getUsersResponse.setLastName(person.getLastName());
			getUsersResponse.setId(Math.toIntExact(person.getId()));

			getUsersResponse.setUsername(person.getUserAccount().getUsername());
			getUsersResponse.setRole(person.getUserAccount().getRoles().toString());

			result.add(getUsersResponse);

		});

		return result;
	}

	public List<Person> getManagers()
	{
		return personRepository.findAll().stream()
				.filter(person -> person.getUserAccount().hasRole(RolesAndPrivileges.MANAGER.name()))
				.collect(Collectors.toList());

	}

	public Person getLoggedPerson(){

		return personRepository.findByUserAccountUsername(getLoggedUsername()).orElseThrow();

	}

	public List<String> getPrivileges(){


		var username = getLoggedUsername();

		return userAccountRepository
				.findByUsername(username)
				.orElseThrow()
				.getAuthorities()
				.stream().map(new Function<GrantedAuthority, String>() {
			@Override
			public String apply(final GrantedAuthority grantedAuthority)
			{
				return grantedAuthority.getAuthority();
			}
		}).collect(Collectors.toList());
	}

	public String getLoggedUsername()
	{
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof DefaultOAuth2User)
		{
			return ((DefaultOAuth2User) principal).getName();
		}
		else
		{
			return  principal.toString();
		}
	}

	public boolean validatePassword(String password){

		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		String actualPassword = getLoggedPerson().getUserAccount().getPassword();

		return passwordEncoder.matches(password, actualPassword);

	}

	public void updatePassword(final String newPassword)
	{
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

		var user = getLoggedPerson().getUserAccount();

		user.setPassword(passwordEncoder.encode(newPassword));

		userAccountRepository.save(user);
	}
}
