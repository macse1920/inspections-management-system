package net.ubtuni.userservice.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreatedPerson
{
	private Person person;
	private String credential;
}
