package net.ubtuni.userservice.models;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;

@Entity
@Data
public class UserAccount implements UserDetails
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(updatable = false, nullable = false, unique = true)
	private String username;

	@Column(updatable = false, nullable = false, unique = true)
	private String emailAddress;

	private String password;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "users_roles",
			joinColumns = @JoinColumn(
					name = "user_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(
					name = "role_id", referencedColumnName = "id"))
	private Collection<Role> roles;


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		Collection<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();

		roles.forEach(role -> role.getPrivileges()
				.forEach(privilege -> simpleGrantedAuthorities.add(new SimpleGrantedAuthority(privilege.getName()))));

		return simpleGrantedAuthorities;
	}

	@Override
	public String getPassword()
	{
		return password;
	}

	@Override
	public String getUsername()
	{
		return username;
	}

	@Override
	public boolean isAccountNonExpired()
	{
		return true;
	}

	@Override
	public boolean isAccountNonLocked()
	{
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired()
	{
		return true;
	}

	@Override
	public boolean isEnabled()
	{
		return true;
	}

	@Override
	public String toString()
	{
		return "UserAccount{" +
				"id=" + id +
				", username='" + username + '\'' +
				", emailAddress='" + emailAddress + '\'' +
				", password='" + password + '\'' +
				", roles=" + roles +
				'}';
	}

	public boolean hasRole(final String name)
	{
		return getRoles().stream().anyMatch(role -> role.getName().equals(name));
	}
}
