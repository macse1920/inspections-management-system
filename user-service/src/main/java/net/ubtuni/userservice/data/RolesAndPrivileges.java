package net.ubtuni.userservice.data;

import static net.ubtuni.userservice.data.RolesAndPrivileges.Privileges.ASSIGN_ZONE_TO_INSPECTOR;
import static net.ubtuni.userservice.data.RolesAndPrivileges.Privileges.CREATE_ADMIN_USERS;
import static net.ubtuni.userservice.data.RolesAndPrivileges.Privileges.CREATE_INSPECTION_CASES;
import static net.ubtuni.userservice.data.RolesAndPrivileges.Privileges.CREATE_INSPECTION_CATEGORIES;
import static net.ubtuni.userservice.data.RolesAndPrivileges.Privileges.CREATE_INSPECTORS;
import static net.ubtuni.userservice.data.RolesAndPrivileges.Privileges.VIEW_ASSIGNED_INSPECTOR_ZONES;
import static net.ubtuni.userservice.data.RolesAndPrivileges.Privileges.VIEW_DASHBOARD;
import static net.ubtuni.userservice.data.RolesAndPrivileges.Privileges.VIEW_INSPECTION_CATEGORIES;
import static net.ubtuni.userservice.data.RolesAndPrivileges.Privileges.VIEW_INSPECTORS;
import static net.ubtuni.userservice.data.RolesAndPrivileges.Privileges.VIEW_INSPECTOR_CASES;

import lombok.Getter;

@Getter
public enum RolesAndPrivileges
{
	ADMIN(VIEW_DASHBOARD, VIEW_INSPECTION_CATEGORIES, VIEW_INSPECTOR_CASES,
			CREATE_ADMIN_USERS, CREATE_INSPECTION_CATEGORIES, VIEW_ASSIGNED_INSPECTOR_ZONES),

	MANAGER(VIEW_DASHBOARD, VIEW_INSPECTORS, VIEW_INSPECTOR_CASES, CREATE_INSPECTORS, CREATE_INSPECTION_CASES,
			VIEW_ASSIGNED_INSPECTOR_ZONES, ASSIGN_ZONE_TO_INSPECTOR),

	INSPECTOR(VIEW_DASHBOARD,  VIEW_INSPECTOR_CASES, CREATE_INSPECTION_CASES, VIEW_INSPECTORS, VIEW_ASSIGNED_INSPECTOR_ZONES);

	private final Privileges[] privileges;

	RolesAndPrivileges(Privileges ... privileges)
	{
		this.privileges = privileges;
	}


	@Getter
	public enum Privileges
	{
		VIEW_DASHBOARD,
		VIEW_INSPECTION_CATEGORIES,
		VIEW_INSPECTORS,
		VIEW_INSPECTOR_CASES,
		VIEW_ASSIGNED_INSPECTOR_ZONES,

		CREATE_ADMIN_USERS,
		CREATE_INSPECTION_CATEGORIES,
		CREATE_INSPECTORS,
		CREATE_INSPECTION_CASES,
		ASSIGN_ZONE_TO_INSPECTOR
	}
}
