package net.ubtuni.resourceserver.peopleDatabase;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.springframework.stereotype.Service;

import net.ubtuni.resourceserver.arbk.service.ARBKService;
import net.ubtuni.resourceserver.arbk.service.models.GetDataByIdentifierResult;

@Service
public class PeopleService
{

	List<PersonInformation> data;

	@PostConstruct
	public void loadData() throws IOException
	{
		var is  = ARBKService.class.getClassLoader().getResourceAsStream("data/person_data.csv");

		BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT);

		var records = csvParser.getRecords();

		data = records.stream().map(record -> PersonInformation
				.builder()
				.identifier(record.get(0))
				.fullName(record.get(1))
				.city(record.get(2))
				.build())
				.collect(Collectors.toList());

	}

	public PersonInformation getInformation(final String identifier){

		return data.stream().filter(personInformation -> personInformation.getIdentifier().equals(identifier))
				.findFirst().orElse(new PersonInformation());

	}
}
