package net.ubtuni.resourceserver.core;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import net.ubtuni.userservice.models.Role;
import net.ubtuni.userservice.service.UserService;

public class BaseController
{
	@Autowired
	protected UserService userService;

	protected List<String> getRoles(){

		return userService.getLoggedPerson()
				.getUserAccount()
				.getRoles()
				.stream().map(Role::getName)
				.collect(Collectors.toList());
	}


}
