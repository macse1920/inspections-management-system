package net.ubtuni.resourceserver.notifications;

import lombok.Data;

@Data
public class EmailModel
{
	private String senderEmail;
	private String subject;
	private String receiverEmail;
}
