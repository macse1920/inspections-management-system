package net.ubtuni.resourceserver.notifications;

public interface NotificationItem<T, U extends EmailNotification>
{
	U buildItem(T t);
}
