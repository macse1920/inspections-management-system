package net.ubtuni.resourceserver.notifications;

public class UserRegistrationNotificationBuilder implements NotificationItem<UserRegistrationEmailModel, EmailNotification>
{
	@Override
	public EmailNotification buildItem(final UserRegistrationEmailModel userRegistrationEmailModel)
	{
		return EmailNotification
				.builder()
				.sender(userRegistrationEmailModel.getSenderEmail())
				.receiver(userRegistrationEmailModel.getReceiverEmail())
				.subject(userRegistrationEmailModel.getSubject())
				.content(userRegistrationEmailModel)
				.build();
	}
}
