package net.ubtuni.resourceserver.notifications;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistrationEmailModel extends EmailModel
{
	private String firstName;
	private String lastName;
	private String username;
	private String password;
}
