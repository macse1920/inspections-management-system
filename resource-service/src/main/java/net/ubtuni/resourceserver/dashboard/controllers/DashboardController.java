package net.ubtuni.resourceserver.dashboard.controllers;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import dashboard.LocationResponse;
import dashboard.ZonesWithActiveCases;
import net.ubtuni.resourceserver.core.BaseController;
import net.ubtuni.resourceserver.dashboard.services.DashboardStatisticsService;
import net.ubtuni.resourceserver.inspections.models.InspectionCase;
import net.ubtuni.resourceserver.utils.Views;

@Controller
public class DashboardController extends BaseController
{
	@Autowired
	private DashboardStatisticsService dashboardStatisticsService;

	@GetMapping("/")
	public ModelAndView index()
	{
		ZonesWithActiveCases zonesWithActiveCases = dashboardStatisticsService.getZonesWithActiveCases();

		var view = Views.INDEX.getView(userService);
		view.addObject("zonesWithActiveCases", zonesWithActiveCases.getZones());

		view.addObject("roles", getRoles());
		view.addObject("inspectorStats", dashboardStatisticsService.getInspectorStatistics());

		return view;
	}

	@GetMapping("/dashboard/data")
	@ResponseBody
	public List<LocationResponse> zones()
	{
		return dashboardStatisticsService.getLocations();
	}

	@GetMapping("/dashboard/inspectors/active-cases")
	@ResponseBody
	public List<InspectionCase> getMyActiveCases(){

		return dashboardStatisticsService.getMyActiveCases();
	}



}
