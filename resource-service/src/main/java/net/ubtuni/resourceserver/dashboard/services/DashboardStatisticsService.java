package net.ubtuni.resourceserver.dashboard.services;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import dashboard.LocationResponse;
import dashboard.ZonesWithActiveCases;
import net.ubtuni.resourceserver.inspections.models.InspectionCase;
import net.ubtuni.resourceserver.inspections.models.InspectorLocationAssignment;
import net.ubtuni.resourceserver.inspections.repositories.InspectionCaseRepository;
import net.ubtuni.resourceserver.inspections.repositories.InspectorLocationAssignmentRepository;
import net.ubtuni.resourceserver.inspections.repositories.InspectorRepository;
import net.ubtuni.resourceserver.locationdata.model.CityZoneInformation;
import net.ubtuni.resourceserver.locationdata.model.StreetInformation;
import net.ubtuni.resourceserver.locationdata.repositories.StreetInformationRepository;
import net.ubtuni.userservice.service.UserService;

@Service
@Repository
public class DashboardStatisticsService
{
	@Autowired
	private InspectorRepository inspectorRepository;

	@Autowired
	private InspectorLocationAssignmentRepository inspectorLocationAssignmentRepository;

	@Autowired
	private StreetInformationRepository streetInformationRepository;

	@Autowired
	private InspectionCaseRepository inspectionCaseRepository;

	@Autowired
	private UserService userService;
	
	@PersistenceContext
	private EntityManager entityManager;

	public ZonesWithActiveCases getZonesWithActiveCases()
	{
		var result = new ZonesWithActiveCases();

		var loggedInspector = inspectorRepository.findByPerson(userService.getLoggedPerson());

		if (loggedInspector.isPresent())
		{

			var inspector = loggedInspector.get();

//			//  Read all assigned Zones for this inspector
//			List<InspectorLocationAssignment> optionalAssignment =
//					inspectorLocationAssignmentRepository.findByInspectorAndIsActive(inspector);
//
//			optionalAssignment.stream().forEach(inspectorLocationAssignment -> {
//
//				ZonesWithActiveCases.Zone zoneResult = new ZonesWithActiveCases.Zone();
//				CityZoneInformation zone = inspectorLocationAssignment.getZone();
//
//
//				// get all streets
//				List<StreetInformation> streets = streetInformationRepository.findByCityZoneInformation(zone);
//
//				streets.forEach(str -> {
//
//					List<InspectionCase> byStreetInformation = inspectionCaseRepository.findByStreetInformation(str);
//
//					if (byStreetInformation.size() > 0)
//					{
//						if(result.getZones().stream().noneMatch(z -> z.getName().equals(zone.getName()))){
//
//							zoneResult.setName(zone.getName());
//							zoneResult.setCity(zone.getCity().getName());
//							zoneResult.setActiveCases(byStreetInformation.size());
//
//						} else{
//							zoneResult.setActiveCases(zoneResult.getActiveCases()+byStreetInformation.size());
//
//						}
//
//
//
//					}
//				});
//
//				result.add(zoneResult);
//
//			});

			List<Object[]> data = entityManager.createNativeQuery("select zone.name, " +
					"       (select COUNT(*) " +
					"        from inspection_case ins " +
					"        where ins.zone in " +
					"              (select id " +
					"               from street_information " +
					"               where zone in (select assignment.zone " +
					"                              from inspector_location_assignment assignment " +
					"                              where assignment.inspector = :inspector " +
					"                                and assignment.zone = zone.name))) as activeCases, " +
					"       zone.city " +
					"from city_zone_information zone " +
					"group by zone.name " +
					"order by activeCases desc " +
					"LIMIT 5")
					.setParameter("inspector", inspector.getId())
					.getResultList();

			data.forEach(record-> {

				ZonesWithActiveCases.Zone z = new ZonesWithActiveCases.Zone();
				z.setName(String.valueOf(record[0]));
				z.setActiveCases(((BigInteger) record[1]).intValue());
				z.setCity(String.valueOf(record[2]));

				result.add(z);
			});


//			result.setZones(resultList);
		}
		return result;
	}

	public List<LocationResponse> getLocations()
	{
		List<LocationResponse> responses = new ArrayList<>();

		var loggedInspector = inspectorRepository.findByPerson(userService.getLoggedPerson());


		if (loggedInspector.isPresent())
		{
			List<InspectorLocationAssignment> byInspectorAndIsActive =
					inspectorLocationAssignmentRepository.findByInspectorAndIsActive(loggedInspector.get());

			for (InspectorLocationAssignment inspectorLocationAssignment : byInspectorAndIsActive)
			{
				List<StreetInformation> streets = streetInformationRepository.findByCityZoneInformation(inspectorLocationAssignment.getZone());

				for (StreetInformation street : streets)
				{
					LocationResponse response = new LocationResponse();
					response.setStreet(street.getName());
					response.setZone(street.getCityZoneInformation().getName());
					response.setCity(street.getCityZoneInformation().getCity().getName());

					responses.add(response);
				}
			}
		}


		return responses;

	}

	public List<InspectionCase> getMyActiveCases()
	{
		List<InspectionCase> result = new ArrayList<>();

		var loggedInspector = inspectorRepository.findByPerson(userService.getLoggedPerson());

		if (loggedInspector.isPresent())
		{
			List<InspectorLocationAssignment> byInspectorAndIsActive =
					inspectorLocationAssignmentRepository.findByInspectorAndIsActive(loggedInspector.get());

			for (InspectorLocationAssignment inspectorLocationAssignment : byInspectorAndIsActive)
			{
				List<StreetInformation> streets = streetInformationRepository.findByCityZoneInformation(inspectorLocationAssignment.getZone());

				for (StreetInformation street : streets)
				{
					result.addAll(inspectionCaseRepository.findByStreetInformation(street));
				}
			}
		}

		return result;
	}

	public HashMap<String, Integer> getInspectorStatistics(){

		HashMap<String,Integer> data = new HashMap<>();

		var loggedInspector = inspectorRepository.findByPerson(userService.getLoggedPerson());

		loggedInspector.ifPresent(inspector -> {

			data.put("numberOfCasesOnZones", inspectionCaseRepository.getCasesForZone(inspector).size());
			data.put("percentageOfCreated", inspectionCaseRepository.getCasesForZone(inspector).size() > 0 ? ((inspectionCaseRepository.getCreatedCasesForZone(inspector).size() /
					inspectionCaseRepository.getCasesForZone(inspector).size()) * 100) : 0);
			data.put("numberOfInspectors", Math.toIntExact(inspectorRepository.count()));






		});

		return data;


	}
}
