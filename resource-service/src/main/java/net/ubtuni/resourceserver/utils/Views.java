package net.ubtuni.resourceserver.utils;

import org.springframework.web.servlet.ModelAndView;

import lombok.Getter;
import net.ubtuni.userservice.service.UserService;

@Getter
public enum Views
{
	INDEX("fragments/dashboard"),
	MANAGE_USERS("manage-users", "redirect:/users/manage-users"),
	MANAGE_INSPECTION_CATEGORIES("manage-inspection-categories", "redirect:/inspections/categories"),
	MANAGE_INSPECTORS("manage-inspectors", "redirect:/inspections/inspectors"),
	MANAGE_INSPECTION_CASES("manage-inspection-cases", "redirect:/inspections/cases"),
	VIEW_INSPECTOR_ASSIGNMENTS("view-inspector-assignment","redirect:/inspections/inspectors/assignment/view/{id}"),
	VIEW_USER_PROFILE("view-profile","redirect:/users/profile");

	private final String path;
	private final String redirectView;

	Views(final String path, final String redirectView)
	{
		this.path = path;
		this.redirectView = redirectView;
	}

	Views(final String path)
	{
		this.path = path;
		this.redirectView = "";
	}

	public ModelAndView getRedirectView(final UserService userService)
	{
		var mv = new ModelAndView(redirectView);
		mv.addObject("privileges", userService.getPrivileges());
		mv.addObject("logged_person", userService.getLoggedPerson());
		return mv;
	}


	public ModelAndView getView(final UserService userService)
	{
		var mv = new ModelAndView(path);
		mv.addObject("privileges", userService.getPrivileges());
		mv.addObject("logged_person", userService.getLoggedPerson());
		return mv;
	}


}
