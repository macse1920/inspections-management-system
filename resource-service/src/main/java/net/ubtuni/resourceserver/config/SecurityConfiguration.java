package net.ubtuni.resourceserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter
{
	@Override
	public void configure(HttpSecurity http) throws Exception
	{

		String[] resources = new String[] {
				"/pictureCheckCode", "/include/**", "/img/**",
				"/css/**", "/icons/**", "/images/**", "/js/**", "/layer/**",
				"/assets/**", "/base/**", "/icons/**", "/notifications/**",
				"/scss/**", "/vendors/**", "/node_modules/**", "/login**",
				"/vendor/**", "/img/**", "/css/**", "/fonts/**", "/js/**"
		};

		http.antMatcher("/**")
				.authorizeRequests()
				.antMatchers(resources).permitAll()
				.anyRequest().authenticated()
				.and()
				.oauth2Login()
				.and()
				.csrf()
				.disable()
				.logout()
				.logoutUrl("/logout")
				.invalidateHttpSession(true)
				.clearAuthentication(true)
				.logoutSuccessUrl("http://localhost:9991/auth/exit")
				.deleteCookies("JSESSIONID", "UISESSION");
	}

//	@Override
//	public void configure(final WebSecurity web) throws Exception
//	{
//		web.authorizeRequests()
//				.antMatchers("/index.html", "/", "/home",
//						"/login","/favicon.ico","/*.js","/*.js.map").permitAll()
//				.anyRequest().authenticated().and().csrf().disable();/ #3
//	}
}
