package net.ubtuni.resourceserver.locationdata.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.resourceserver.locationdata.model.CityZoneInformation;

public interface CityZoneInformationRepository extends JpaRepository<CityZoneInformation, Long>
{
	boolean existsByName(String name);

	Optional<CityZoneInformation> findByName(String name);
}
