package net.ubtuni.resourceserver.locationdata.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.resourceserver.locationdata.model.CityInformation;

public interface CityInformationRepository extends JpaRepository<CityInformation, Long>
{
	boolean existsByName(String name);

	Optional<CityInformation> findByName(String name);
}
