package net.ubtuni.resourceserver.locationdata.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.resourceserver.locationdata.model.CityZoneInformation;
import net.ubtuni.resourceserver.locationdata.model.StreetInformation;

public interface StreetInformationRepository extends JpaRepository<StreetInformation, Long>
{
	 List<StreetInformation> findByCityZoneInformation(CityZoneInformation cityZoneInformation);
}
