package net.ubtuni.resourceserver.locationdata.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Entity
@Data
public class CityZoneInformation
{
	@Id
	private String name;

	@ManyToOne
	@JoinColumn(name = "city")
	private CityInformation city;
}
