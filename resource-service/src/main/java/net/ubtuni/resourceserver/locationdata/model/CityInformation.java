package net.ubtuni.resourceserver.locationdata.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class CityInformation
{
	@Id
	private String name;
}
