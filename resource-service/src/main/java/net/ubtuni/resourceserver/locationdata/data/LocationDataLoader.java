package net.ubtuni.resourceserver.locationdata.data;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import com.github.javafaker.Faker;
import net.ubtuni.resourceserver.locationdata.model.CityInformation;
import net.ubtuni.resourceserver.locationdata.model.CityZoneInformation;
import net.ubtuni.resourceserver.locationdata.model.StreetInformation;
import net.ubtuni.resourceserver.locationdata.repositories.CityInformationRepository;
import net.ubtuni.resourceserver.locationdata.repositories.CityZoneInformationRepository;
import net.ubtuni.resourceserver.locationdata.repositories.StreetInformationRepository;

@Component
public class LocationDataLoader implements CommandLineRunner
{
	@Autowired
	private CityInformationRepository cityInformationRepository;

	@Autowired
	private CityZoneInformationRepository cityZoneInformationRepository;

	@Autowired
	private StreetInformationRepository streetInformationRepository;

	public static final List<String> CITIES = Collections.singletonList("Prishtine");
	public static final List<String> ZONES = Arrays.asList("Ulpiana", "Dardania", "Arbëria", "Kodra e Diellit",
			" Kolovica", "Sofali", "Mati 1", "Emshir", "Kodra e Trimave", "Përroi i njelmët", "Vreshta");

	@Override
	public void run(final String... args) throws Exception
	{
		// store cities
		for (String city : CITIES)
		{
			if (!cityInformationRepository.existsByName(city))
			{
				var cityInformation = new CityInformation();
				cityInformation.setName(city);
				cityInformationRepository.save(cityInformation);
			}
		}

		// store zones
		var city = cityInformationRepository.findByName(CITIES.get(0)).orElseThrow();

		for (String zone : ZONES)
		{
			if (!cityZoneInformationRepository.existsByName(zone))
			{
				CityZoneInformation cityZoneInformation = new CityZoneInformation();
				cityZoneInformation.setCity(city);
				cityZoneInformation.setName(zone);
				cityZoneInformationRepository.save(cityZoneInformation);
			}
		}

		cityZoneInformationRepository.findAll()
				.forEach(cityZoneInformation -> {


					StreetInformation streetInformation = new StreetInformation();
					Faker faker = new Faker();
					streetInformation.setName("Rr. "+ faker.address().streetAddress());
					streetInformation.setCityZoneInformation(cityZoneInformation);

					streetInformationRepository.save(streetInformation);


				});

	}


}
