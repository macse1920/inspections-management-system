package net.ubtuni.resourceserver.userManagement.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import net.ubtuni.resourceserver.userManagement.UserNotificationService;
import net.ubtuni.resourceserver.utils.Views;
import net.ubtuni.userservice.service.RoleService;
import net.ubtuni.userservice.service.UserService;
import users.ChangePasswordRequest;
import users.CreateUserRequest;
import users.GetUsersResponse;


@Controller
@RequestMapping("/users")
@ComponentScan(basePackages = "net.ubtuni.userservice")
public class UserController
{
	private final UserService userService;
	private final RoleService roleService;
	private final UserNotificationService userNotificationService;

	public UserController(final UserService userService, final RoleService roleService,
			final UserNotificationService userNotificationService)
	{
		this.userService = userService;
		this.roleService = roleService;
		this.userNotificationService = userNotificationService;
	}

	@GetMapping({"/profile", "/profile/password"})
	public ModelAndView viewUserProfile()
	{
		var mav = Views.VIEW_USER_PROFILE.getView(userService);
		mav.addObject("user", userService.getLoggedPerson());
		List<String> errors = new ArrayList<>();
		mav.addObject("errors", errors);

		return mav;
	}

	@PostMapping("/profile/password")
	public ModelAndView changePassword(@ModelAttribute ChangePasswordRequest request)
	{
		var redirectView = Views.VIEW_USER_PROFILE.getView(userService);
		redirectView.addObject("user", userService.getLoggedPerson());

		List<String> errors = new ArrayList<>();

		var validPassword = userService.validatePassword(request.getOldPassword());

		if (!validPassword)
		{
			errors.add("oldPassword");
		}
		else
		{
			userService.updatePassword(request.getNewPassword());
		}

		redirectView.addObject("errors", errors);
		return redirectView;


	}

	@PostMapping
	public ModelAndView createUser(@Valid @ModelAttribute CreateUserRequest createUserRequest)
	{

		var createdPerson = userService.createUser(createUserRequest, new BCryptPasswordEncoder());

		userNotificationService.notifyUser(createdPerson);

		var mav = Views.MANAGE_USERS.getRedirectView(userService);

		populateManageUsersData(mav);

		return mav;
	}


	@GetMapping("/manage-users")
	public ModelAndView manageUsers()
	{

		var mav = Views.MANAGE_USERS.getView(userService);

		populateManageUsersData(mav);

		return mav;
	}

	@GetMapping("/data")
	public @ResponseBody
	List<GetUsersResponse> getUsers()
	{
		return userService.getUsers();
	}

	private void populateManageUsersData(final ModelAndView mav)
	{
		mav.addObject("data", userService.getUsers());
		mav.addObject("userToCreate", new CreateUserRequest());

		mav.addObject("availableRoles", roleService.getAvailableRoles());

	}
}
