package net.ubtuni.resourceserver.userManagement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ubtuni.resourceserver.notifications.EmailNotificationService;
import net.ubtuni.resourceserver.notifications.UserRegistrationEmailModel;
import net.ubtuni.resourceserver.notifications.UserRegistrationNotificationBuilder;
import net.ubtuni.userservice.models.CreatedPerson;
import net.ubtuni.userservice.models.Person;

@Service
public class UserNotificationService
{

	@Autowired
	private EmailNotificationService emailNotificationService;

	public void notifyUser(final CreatedPerson createdPerson){


		var notificationItem = new UserRegistrationNotificationBuilder();

		var emailModel = new UserRegistrationEmailModel();
		emailModel.setSenderEmail("notify@appName.com");
		emailModel.setReceiverEmail(createdPerson.getPerson().getUserAccount().getEmailAddress());
		emailModel.setPassword(createdPerson.getCredential());
		emailModel.setSubject("Welcome");
		emailModel.setUsername(createdPerson.getPerson().getUserAccount().getUsername());
		emailModel.setFirstName(createdPerson.getPerson().getFirstName());
		emailModel.setLastName(createdPerson.getPerson().getLastName());

		emailNotificationService.sendEmail(notificationItem.buildItem(emailModel));


	}
}
