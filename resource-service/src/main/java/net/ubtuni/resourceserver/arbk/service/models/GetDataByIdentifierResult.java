package net.ubtuni.resourceserver.arbk.service.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetDataByIdentifierResult
{
	private String businessName;
	private String businessType;
	private String uniqueIdentifier;
	private String registrationDate;
	private String municipality ;
	private String address;
}
