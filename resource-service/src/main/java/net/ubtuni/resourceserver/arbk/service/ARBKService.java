package net.ubtuni.resourceserver.arbk.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.springframework.stereotype.Service;

import net.ubtuni.resourceserver.arbk.service.models.GetDataByIdentifierResult;

@Service
public class ARBKService
{

	List<GetDataByIdentifierResult> data;

	@PostConstruct
	public void loadData() throws IOException
	{
		var is  = ARBKService.class.getClassLoader().getResourceAsStream("data/business_data.csv");

		BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		CSVParser csvParser = new CSVParser(fileReader, CSVFormat.DEFAULT);

		var records = csvParser.getRecords();

		data = records.stream().map(record -> GetDataByIdentifierResult
				.builder()
				.businessName(record.get(0))
				.registrationDate(record.get(1))
				.address(record.get(2))
				.businessType(record.get(3))
				.municipality(record.get(4))
				.uniqueIdentifier(record.get(5))
				.build())
				.collect(Collectors.toList());

	}

	public GetDataByIdentifierResult executeQuery(String identifier)
	{
		return data.stream()
				.filter(getDataByIdentifierResult -> getDataByIdentifierResult.getUniqueIdentifier().equals(identifier))
				.findFirst().orElse(new GetDataByIdentifierResult());
	}
}
