package net.ubtuni.resourceserver.inspections.models;

public enum InspectionCaseReporterType
{
	INTERNAL, EXTERNAL
}
