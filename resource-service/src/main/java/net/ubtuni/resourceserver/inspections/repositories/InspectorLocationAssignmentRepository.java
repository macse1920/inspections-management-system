package net.ubtuni.resourceserver.inspections.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import net.ubtuni.resourceserver.inspections.models.Inspector;
import net.ubtuni.resourceserver.inspections.models.InspectorLocationAssignment;
import net.ubtuni.resourceserver.locationdata.model.CityZoneInformation;

public interface InspectorLocationAssignmentRepository extends JpaRepository<InspectorLocationAssignment, Long >
{
	List<InspectorLocationAssignment> findByInspectorId(final long inspectorId);

//	List<InspectorLocationAssignment> findByInspectorIdAndActive(final long inspectorId, final boolean status);

	@Query(value = "SELECT i FROM InspectorLocationAssignment i where i.inspector = :inspector and i.isActive=true")
	List<InspectorLocationAssignment> findByInspectorAndIsActive(Inspector inspector);


	Optional<InspectorLocationAssignment> findByInspectorAndZone(final Inspector inspector, final CityZoneInformation cityZoneInformation);

}
