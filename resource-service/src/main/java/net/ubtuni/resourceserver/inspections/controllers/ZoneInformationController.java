package net.ubtuni.resourceserver.inspections.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.ubtuni.resourceserver.inspections.services.LocationService;
import net.ubtuni.resourceserver.locationdata.model.CityInformation;
import net.ubtuni.resourceserver.locationdata.model.CityZoneInformation;
import net.ubtuni.resourceserver.locationdata.model.StreetInformation;

@Controller
@RequestMapping(value = "/locations/")
public class ZoneInformationController
{
	@Autowired
	private LocationService locationService;
//
//	@GetMapping(value = "cities")
//	@ResponseBody
//	public List<CityInformation> getCities(){
//
//		return locationService.getCities();
//
//	}

	@GetMapping("{city}")
	@ResponseBody
	public List<CityZoneInformation> getZones(@PathVariable String city){

		return locationService.getZones(city);

	}

	@GetMapping("streets/{zone}")
	@ResponseBody
	public List<StreetInformation> getStreets(@PathVariable String zone){

		return locationService.getStreets(zone);
	}


}
