package net.ubtuni.resourceserver.inspections.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.Data;
import net.ubtuni.userservice.models.Person;

@Entity
@Data
public class InspectionCategory
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;

	@OneToOne
	private Person person;


}
