package net.ubtuni.resourceserver.inspections.services;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import inspectionCases.CreateInspectionCaseRequest;
import inspectionCases.DetailedInspectionCase;
import inspectionCases.InspectionCaseSummary;
import net.ubtuni.resourceserver.arbk.service.ARBKService;
import net.ubtuni.resourceserver.arbk.service.models.GetDataByIdentifierResult;
import net.ubtuni.resourceserver.inspections.models.InspectionCase;
import net.ubtuni.resourceserver.inspections.models.InspectionCaseAccusedParty;
import net.ubtuni.resourceserver.inspections.models.InspectionCaseAccusedPartyType;
import net.ubtuni.resourceserver.inspections.models.InspectionCaseAttachment;
import net.ubtuni.resourceserver.inspections.models.InspectionCaseReporter;
import net.ubtuni.resourceserver.inspections.models.InspectionCaseReporterType;
import net.ubtuni.resourceserver.inspections.models.InspectionCaseStatus;
import net.ubtuni.resourceserver.inspections.repositories.InspectionCaseAccusedPartyRepository;
import net.ubtuni.resourceserver.inspections.repositories.InspectionCaseAttachmentRepository;
import net.ubtuni.resourceserver.inspections.repositories.InspectionCaseReporterRepository;
import net.ubtuni.resourceserver.inspections.repositories.InspectionCaseRepository;
import net.ubtuni.resourceserver.locationdata.repositories.StreetInformationRepository;
import net.ubtuni.resourceserver.peopleDatabase.PeopleService;
import net.ubtuni.resourceserver.peopleDatabase.PersonInformation;

@Service
public class InspectionCaseService
{
	@Autowired
	private InspectionCaseRepository inspectionCaseRepository;

	@Autowired
	private InspectionCaseReporterRepository inspectionCaseReporterRepository;

	@Autowired
	private InspectionCaseAccusedPartyRepository inspectionCaseAccusedPartyRepository;

	@Autowired
	private StreetInformationRepository streetInformationRepository;

	@Autowired
	private ARBKService arbkService;

	@Autowired
	private PeopleService peopleService;

	@Autowired
	private InspectionCaseAttachmentRepository inspectionCaseAttachmentRepository;

	public InspectionCase createInspectionCase(final CreateInspectionCaseRequest createInspectionCaseRequest)
	{
		final InspectionCase inspectionCase = new InspectionCase();
		inspectionCase.setStatus(InspectionCaseStatus.CREATED);

		if (createInspectionCaseRequest.getReporterIdentifier().isBlank())
		{
			inspectionCase.setInspectionCaseReporterType(InspectionCaseReporterType.INTERNAL);
		}
		else
		{
			inspectionCase.setInspectionCaseReporterType(InspectionCaseReporterType.EXTERNAL);

			var optionalReporter = inspectionCaseReporterRepository.
					findByIdentifier(createInspectionCaseRequest.getReporterIdentifier());

			if (optionalReporter.isEmpty())
			{
				PersonInformation individualData = getIndividualData(createInspectionCaseRequest.getReporterIdentifier());

				var inspectionCaseReporterToCreate = new InspectionCaseReporter();
				inspectionCaseReporterToCreate.setIdentifier(individualData.getIdentifier());
				inspectionCaseReporterToCreate.setName(individualData.getFullName());

				inspectionCase.setReporter(inspectionCaseReporterRepository.save(inspectionCaseReporterToCreate));

			}
			else
			{
				inspectionCase.setReporter(optionalReporter.get());
			}
		}


		if (!createInspectionCaseRequest.getAccusedIdentifier().isBlank())
		{
			if (createInspectionCaseRequest.isIndividual())
			{
				PersonInformation individualData = getIndividualData(createInspectionCaseRequest.getAccusedIdentifier());

				var inspectionCaseAccusedPartyToCreate = new InspectionCaseAccusedParty();
				inspectionCaseAccusedPartyToCreate.setInspectionCaseAccusedPartyType(InspectionCaseAccusedPartyType.INDIVIDUAL);
				inspectionCaseAccusedPartyToCreate.setIdentifier(individualData.getIdentifier());

				inspectionCase.setInspectionCaseAccusedParty(inspectionCaseAccusedPartyRepository.save(inspectionCaseAccusedPartyToCreate));

			}
			else
			{
				var optionalAcussedParty = inspectionCaseAccusedPartyRepository
						.findByIdentifier(createInspectionCaseRequest.getAccusedIdentifier());

				if (optionalAcussedParty.isPresent())
				{
					inspectionCase.setInspectionCaseAccusedParty(optionalAcussedParty.get());
				}
				else
				{
					var arbkResult = arbkService.executeQuery(createInspectionCaseRequest.getAccusedIdentifier());

					var inspectionCaseAccusedPartyToCreate = new InspectionCaseAccusedParty();
					inspectionCaseAccusedPartyToCreate.setInspectionCaseAccusedPartyType(InspectionCaseAccusedPartyType.BUSINESS_ENTITY);
					inspectionCaseAccusedPartyToCreate.setIdentifier(arbkResult.getUniqueIdentifier());

					inspectionCase.setInspectionCaseAccusedParty(inspectionCaseAccusedPartyRepository.save(inspectionCaseAccusedPartyToCreate));
				}


			}


		}

		inspectionCase.setTitle(createInspectionCaseRequest.getTitle());
		inspectionCase.setDescription(createInspectionCaseRequest.getDescription());

		if (createInspectionCaseRequest.getStreet() > 0)
		{
			var streetInformation = streetInformationRepository.findById((long) createInspectionCaseRequest.getStreet())
					.orElseThrow();

			inspectionCase.setStreetInformation(streetInformation);

		}

		return inspectionCaseRepository.save(inspectionCase);


	}

	public GetDataByIdentifierResult getBusinessData(final String identifier)
	{

		return arbkService.executeQuery(identifier);
	}


	public PersonInformation getIndividualData(final String searchCriteria)
	{
		return peopleService.getInformation(searchCriteria);
	}

	public List<InspectionCaseSummary> getSummary()
	{
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

		return inspectionCaseRepository
				.findAll().stream()
				.map(inspectionCase -> {
					InspectionCaseSummary inspectionCaseSummary = new InspectionCaseSummary();
					inspectionCaseSummary.setStatus(inspectionCase.getStatus().toString());
					inspectionCaseSummary.setId(String.valueOf(inspectionCase.getId()));
					inspectionCaseSummary.setTitle(inspectionCase.getTitle());
					inspectionCaseSummary.setAttachments(inspectionCaseAttachmentRepository.countAllByInspectionCase(inspectionCase));
					inspectionCaseSummary.setReportedAt(formatter.format(inspectionCase.getReportedAt()));
					inspectionCaseSummary.setReportedBy(inspectionCase.getReporter() != null ? inspectionCase.getReporter().getName() : "");
					return inspectionCaseSummary;
				}).collect(Collectors.toList());
	}

	public DetailedInspectionCase getDetailedView(final long id)
	{
		final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

		return inspectionCaseRepository
				.findById(id)
				.map(inspectionCase -> {

					DetailedInspectionCase detailedInspectionCase = new DetailedInspectionCase();
					detailedInspectionCase.setTitle(inspectionCase.getTitle());
					detailedInspectionCase.setDescription(inspectionCase.getDescription());
					detailedInspectionCase.setReporterName(inspectionCase.getReporter() != null ? inspectionCase.getReporter().getName() : "");
					detailedInspectionCase.setId(String.valueOf(inspectionCase.getId()));
					detailedInspectionCase.setAccusedType(inspectionCase.getInspectionCaseAccusedParty().getInspectionCaseAccusedPartyType().name());
					detailedInspectionCase.setReportedAt(formatter.format(inspectionCase.getReportedAt()));

					switch(inspectionCase.getInspectionCaseAccusedParty().getInspectionCaseAccusedPartyType())
					{
						case INDIVIDUAL:

							var personalDetail = peopleService.getInformation(inspectionCase.getInspectionCaseAccusedParty().getIdentifier());
							detailedInspectionCase.setAccusedEntityId(personalDetail.getIdentifier());
							detailedInspectionCase.setAccusedEntityName(personalDetail.getFullName());
							detailedInspectionCase.setAccusedEntityCity(personalDetail.getCity());
							break;
						default:

							var businessDetail = arbkService.executeQuery(inspectionCase.getInspectionCaseAccusedParty().getIdentifier());
							detailedInspectionCase.setAccusedEntityId(businessDetail.getUniqueIdentifier());
							detailedInspectionCase.setAccusedEntityName(businessDetail.getBusinessName());
							detailedInspectionCase.setAccusedEntityCity(businessDetail.getMunicipality());
							break;
					}

					return detailedInspectionCase;


				})
				.orElseThrow();
	}
}
