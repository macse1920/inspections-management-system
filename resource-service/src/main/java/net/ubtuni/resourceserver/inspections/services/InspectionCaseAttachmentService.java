package net.ubtuni.resourceserver.inspections.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import net.ubtuni.resourceserver.inspections.models.InspectionCase;
import net.ubtuni.resourceserver.inspections.models.InspectionCaseAttachment;
import net.ubtuni.resourceserver.inspections.repositories.InspectionCaseAttachmentRepository;

@Service
public class InspectionCaseAttachmentService
{
	@Autowired
	private InspectionCaseAttachmentRepository inspectionCaseAttachmentRepository;

	public InspectionCaseAttachment saveFile(MultipartFile multipartFile, InspectionCase inspectionCase)
	{
		var file = new InspectionCaseAttachment();

		file.setName(multipartFile.getOriginalFilename());
		try
		{
			file.setContent(multipartFile.getBytes());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		file.setInspectionCase(inspectionCase);

		return inspectionCaseAttachmentRepository.save(file);
	}
}
