package net.ubtuni.resourceserver.inspections.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import inspectionAssignment.GetAssignmentData;
import inspectionAssignment.UpdateInspectorAssignment;
import net.ubtuni.resourceserver.inspections.models.InspectorLocationAssignment;
import net.ubtuni.resourceserver.inspections.repositories.InspectorLocationAssignmentRepository;
import net.ubtuni.resourceserver.inspections.repositories.InspectorRepository;
import net.ubtuni.resourceserver.locationdata.repositories.CityZoneInformationRepository;

@Service
public class InspectorAssignmentService
{
	@Autowired
	private InspectorLocationAssignmentRepository inspectorLocationAssignmentRepository;

	@Autowired
	private InspectorRepository inspectorRepository;

	@Autowired
	private CityZoneInformationRepository cityZoneInformationRepository;

	public List<GetAssignmentData> getData(final int id)
	{
		return inspectorLocationAssignmentRepository.findByInspectorId(id)
				.stream()
				.map(inspectorLocationAssignment -> GetAssignmentData.builder()
						.zone(inspectorLocationAssignment.getZone().getName() + " ( " + inspectorLocationAssignment.getZone().getCity().getName() + " )")
						.assigned(inspectorLocationAssignment.isActive())
						.build())
				.collect(Collectors.toList());
	}

	public void updateAssignment(final UpdateInspectorAssignment assignmentRequest)
	{
		var inspector = inspectorRepository.findById(assignmentRequest.getInspectorId()).orElseThrow();

		var zone = cityZoneInformationRepository.findByName(assignmentRequest.getZoneId()).orElseThrow();

		var optionalResult = inspectorLocationAssignmentRepository.findByInspectorAndZone(inspector, zone);

		if (assignmentRequest.isAssigned())
		{
			if (optionalResult.isEmpty())
			{
				var inspectorLocationAssignment = new InspectorLocationAssignment();
				inspectorLocationAssignment.setInspector(inspector);
				inspectorLocationAssignment.setZone(zone);
				inspectorLocationAssignment.setActive(true);

				inspectorLocationAssignmentRepository.save(inspectorLocationAssignment);
			}


		}
		else
		{
			optionalResult.ifPresent(data -> {

				data.setActive(false);

				inspectorLocationAssignmentRepository.save(data);

			});
		}
	}
}
