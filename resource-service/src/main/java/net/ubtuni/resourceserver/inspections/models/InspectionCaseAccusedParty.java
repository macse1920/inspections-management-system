package net.ubtuni.resourceserver.inspections.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class InspectionCaseAccusedParty
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String identifier;

	@Enumerated(EnumType.STRING)
	private InspectionCaseAccusedPartyType inspectionCaseAccusedPartyType;
}
