package net.ubtuni.resourceserver.inspections.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class InspectionCaseReporter
{
	@Id
	@GeneratedValue
	private Long id;

	private String identifier;

	private String name;

	private String organization;


}
