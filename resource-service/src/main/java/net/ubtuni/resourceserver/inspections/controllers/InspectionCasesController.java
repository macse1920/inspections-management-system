package net.ubtuni.resourceserver.inspections.controllers;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import inspectionCases.CreateInspectionCaseRequest;
import inspectionCases.DetailedInspectionCase;
import inspectionCases.InspectionCaseSummary;
import net.ubtuni.resourceserver.arbk.service.models.GetDataByIdentifierResult;
import net.ubtuni.resourceserver.core.BaseController;
import net.ubtuni.resourceserver.inspections.models.InspectionCase;
import net.ubtuni.resourceserver.inspections.services.InspectionCaseAttachmentService;
import net.ubtuni.resourceserver.inspections.services.InspectionCaseService;
import net.ubtuni.resourceserver.inspections.services.LocationService;
import net.ubtuni.resourceserver.peopleDatabase.PersonInformation;
import net.ubtuni.resourceserver.utils.Views;

@Controller
@RequestMapping(value = "/inspections/cases")
public class InspectionCasesController extends BaseController
{
	@Autowired
	private InspectionCaseService inspectionCaseService;

	@Autowired
	private LocationService locationService;

	@Autowired
	private InspectionCaseAttachmentService inspectionCaseAttachmentService;

	@GetMapping
	public ModelAndView getCases(){

		ModelAndView view = Views.MANAGE_INSPECTION_CASES.getView(userService);
		view.addObject("cities", locationService.getCities());
		view.addObject("caseToCreate", new CreateInspectionCaseRequest());
//		view.addObject("managers", inspectionCategoryService.getAvailableManagers());


		return view;
	}

	@GetMapping("/summary/data")
	@ResponseBody
	public List<InspectionCaseSummary> getSummaryData(){

		return inspectionCaseService.getSummary();
	}

	@PostMapping
	public ModelAndView createCase(@ModelAttribute CreateInspectionCaseRequest request, @RequestParam("file") MultipartFile [] files){

		InspectionCase inspectionCase = inspectionCaseService.createInspectionCase(request);

		for (MultipartFile file : files)
		{
			inspectionCaseAttachmentService.saveFile(file, inspectionCase);
		}

		ModelAndView view = Views.MANAGE_INSPECTION_CASES.getRedirectView(userService);
		view.addObject("caseToCreate", new CreateInspectionCaseRequest());
//		view.addObject("managers", inspectionCategoryService.getAvailableManagers());


		return view;
	}



	@GetMapping("/business-data/{searchCriteria}")
	@ResponseBody
	public List<GetDataByIdentifierResult> getBusinessData(@PathVariable String searchCriteria){

		return Collections.singletonList(inspectionCaseService.getBusinessData(searchCriteria));

	}

	@GetMapping("/individual-data/{searchCriteria}")
	@ResponseBody
	public List<PersonInformation> getIndividualData(@PathVariable String searchCriteria){

		return Collections.singletonList(inspectionCaseService.getIndividualData(searchCriteria));

	}

	@GetMapping("/detailed/data/{id}")
	@ResponseBody
	public DetailedInspectionCase getDetailedInspectionCase(@PathVariable long id){

		return inspectionCaseService.getDetailedView(id);

	}
}
