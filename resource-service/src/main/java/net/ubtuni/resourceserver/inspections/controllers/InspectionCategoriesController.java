package net.ubtuni.resourceserver.inspections.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import inspectionCategories.CategoryListResponse;
import inspectionCategories.CreateInspectionCategoryRequest;
import net.ubtuni.resourceserver.inspections.services.InspectionCategoryService;
import net.ubtuni.resourceserver.utils.Views;
import net.ubtuni.userservice.service.UserService;

@Controller
@RequestMapping(value = "/inspections/categories")
public class InspectionCategoriesController
{

	@Autowired
	private InspectionCategoryService inspectionCategoryService;

	@Autowired
	private UserService userService;

	@GetMapping
	public ModelAndView viewCategories()
	{
		ModelAndView view = Views.MANAGE_INSPECTION_CATEGORIES.getView(userService);
		view.addObject("categoryToCreate", new CreateInspectionCategoryRequest());
		view.addObject("managers", inspectionCategoryService.getAvailableManagers());


		return view;
	}

	@PostMapping
	public ModelAndView createCategory(@Valid @ModelAttribute CreateInspectionCategoryRequest request)
	{
		inspectionCategoryService.createInspectionCategory(request);

		ModelAndView view = Views.MANAGE_INSPECTION_CATEGORIES.getRedirectView(userService);
		view.addObject("categoryToCreate", new CreateInspectionCategoryRequest());
		view.addObject("managers", inspectionCategoryService.getAvailableManagers());

		return view;
	}

	@GetMapping("/data")
	public @ResponseBody List<CategoryListResponse> getData()
	{
		return inspectionCategoryService.getCategories();
	}
}
