package net.ubtuni.resourceserver.inspections.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import inspectors.CreateInspectorRequest;
import inspectors.GetInspectorResponse;
import net.ubtuni.resourceserver.inspections.models.Inspector;
import net.ubtuni.resourceserver.inspections.repositories.InspectionCategoryRepository;
import net.ubtuni.resourceserver.inspections.repositories.InspectorRepository;
import net.ubtuni.resourceserver.notifications.EmailNotificationService;
import net.ubtuni.resourceserver.userManagement.UserNotificationService;
import net.ubtuni.userservice.data.RolesAndPrivileges;
import net.ubtuni.userservice.models.Person;
import net.ubtuni.userservice.repositories.PersonRepository;
import net.ubtuni.userservice.service.UserService;
import users.CreateUserRequest;

@Service
public class InspectorService
{
	@Autowired
	private InspectorRepository inspectorRepository;

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private InspectionCategoryRepository inspectionCategoryRepository;

	@Autowired
	private UserNotificationService userNotificationService;

	@Autowired
	private UserService userService;

	public void createInspector(final CreateInspectorRequest createInspectorRequest)
	{
		CreateUserRequest createUserRequest = new CreateUserRequest();
		createUserRequest.setUsername(createInspectorRequest.getUsername());
		createUserRequest.setFirstName(createInspectorRequest.getFirstName());
		createUserRequest.setLastName(createInspectorRequest.getLastName());
		createUserRequest.setEmailAddress(createInspectorRequest.getEmailAddress());
		createUserRequest.setRole(RolesAndPrivileges.INSPECTOR.name());

		var result = userService.createUser(createUserRequest, new BCryptPasswordEncoder());
		userNotificationService.notifyUser(result);


		Person user = result.getPerson();

		var person = personRepository.findById(user.getId()).orElseThrow();

		var inspectionCategory = inspectionCategoryRepository.findById(createInspectorRequest.getCategoryId()).orElseThrow();

		Inspector inspector = new Inspector();
		inspector.setCategory(inspectionCategory);
		inspector.setPerson(person);

		inspectorRepository.save(inspector);
	}

	public List<GetInspectorResponse> getInspectors()
	{
		return inspectorRepository.findAll().stream()
				.map(inspector -> {
					GetInspectorResponse getInspectorResponse = new GetInspectorResponse();
					getInspectorResponse.setId(Math.toIntExact(inspector.getId()));
					getInspectorResponse.setFullName(inspector.getPerson().getFirstName() + " " + inspector.getPerson().getLastName());
					getInspectorResponse.setCategory(inspector.getCategory().getName());
					getInspectorResponse.setManager(inspector.getCategory().getPerson().getFirstName()+" "+ inspector.getCategory().getPerson().getLastName());
					return getInspectorResponse;
				}).collect(Collectors.toList());
	}


}
