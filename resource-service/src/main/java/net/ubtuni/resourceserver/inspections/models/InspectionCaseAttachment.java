package net.ubtuni.resourceserver.inspections.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "inspection_case_attachment")
public class InspectionCaseAttachment
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name;

	@Lob
	private byte[] content;

	@ManyToOne
	@JoinColumn(name = "inspectionCase")
	private InspectionCase inspectionCase;
}
