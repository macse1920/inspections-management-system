package net.ubtuni.resourceserver.inspections.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import inspectionCategories.CategoryListResponse;
import inspectionCategories.CreateInspectionCategoryRequest;
import net.ubtuni.resourceserver.inspections.models.InspectionCategory;
import net.ubtuni.resourceserver.inspections.repositories.InspectionCategoryRepository;
import net.ubtuni.userservice.data.RolesAndPrivileges;
import net.ubtuni.userservice.models.Person;
import net.ubtuni.userservice.repositories.PersonRepository;
import net.ubtuni.userservice.repositories.UserAccountRepository;

@Service
public class InspectionCategoryService
{
	private final InspectionCategoryRepository inspectionCategoryRepository;
	private UserAccountRepository userAccountRepository;
	private final PersonRepository personRepository;

	public InspectionCategoryService(final InspectionCategoryRepository inspectionCategoryRepository,
			final UserAccountRepository userAccountRepository,
			final PersonRepository personRepository)
	{
		this.inspectionCategoryRepository = inspectionCategoryRepository;
		this.userAccountRepository = userAccountRepository;
		this.personRepository = personRepository;
	}


	public void createInspectionCategory(final CreateInspectionCategoryRequest createInspectionCategoryRequest)
	{
		var existingManager = personRepository.findById(createInspectionCategoryRequest.getManagerId())
				.orElseThrow().getUserAccount();

		if (!existingManager.hasRole(RolesAndPrivileges.MANAGER.name()))
		{
			throw new RuntimeException("No access"); // TODO: 24/05/2021 update exception
		}

		InspectionCategory inspectionCategory = new InspectionCategory();
		inspectionCategory.setName(createInspectionCategoryRequest.getName());
		inspectionCategory.setPerson(personRepository.findByUserAccountUsername(existingManager.getUsername()).orElseThrow());

		inspectionCategoryRepository.save(inspectionCategory);
	}


	public List<CategoryListResponse> getCategories()
	{

		return inspectionCategoryRepository.findAll()
				.stream().map(inspectionCategory -> {
					CategoryListResponse categoryListResponse = new CategoryListResponse();
					categoryListResponse.setId(Math.toIntExact(inspectionCategory.getId()));
					categoryListResponse.setName(inspectionCategory.getName());
					categoryListResponse.setOwnerFullName(inspectionCategory.getPerson().getFirstName() + " " +
							inspectionCategory.getPerson().getLastName());
					return categoryListResponse;

				})
				.collect(Collectors.toList());

	}

	public boolean isManagerAvailable(Person person)
	{
		return inspectionCategoryRepository.findByPerson(person).isEmpty();
	}

	public List<Person> getAvailableManagers()
	{
		return personRepository.findAll()
				.stream()
				.filter(person -> person.getUserAccount().hasRole(RolesAndPrivileges.MANAGER.name()))
				.filter(this::isManagerAvailable)
				.collect(Collectors.toList());
	}
}
