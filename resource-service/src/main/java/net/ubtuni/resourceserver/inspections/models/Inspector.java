package net.ubtuni.resourceserver.inspections.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;
import net.ubtuni.userservice.models.Person;

@Data
@Entity
public class Inspector
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@OneToOne
	@JoinColumn(name = "person")
	private Person person;

	@OneToOne
	@JoinColumn(name = "inspection_category")
	private InspectionCategory category;
}
