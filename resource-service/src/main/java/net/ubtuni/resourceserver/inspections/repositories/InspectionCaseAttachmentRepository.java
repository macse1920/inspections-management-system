package net.ubtuni.resourceserver.inspections.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.resourceserver.inspections.models.InspectionCase;
import net.ubtuni.resourceserver.inspections.models.InspectionCaseAttachment;

public interface InspectionCaseAttachmentRepository extends JpaRepository<InspectionCaseAttachment, Long>
{
	int countAllByInspectionCase(InspectionCase inspectionCase);
}
