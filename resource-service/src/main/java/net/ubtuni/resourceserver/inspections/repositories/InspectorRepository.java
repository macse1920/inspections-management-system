package net.ubtuni.resourceserver.inspections.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.resourceserver.inspections.models.Inspector;
import net.ubtuni.userservice.models.Person;

public interface InspectorRepository extends JpaRepository<Inspector, Long>
{
	Optional<Inspector> findByPerson(Person person);
}
