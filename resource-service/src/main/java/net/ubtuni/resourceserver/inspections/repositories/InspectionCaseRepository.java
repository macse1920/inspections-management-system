package net.ubtuni.resourceserver.inspections.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import net.ubtuni.resourceserver.inspections.models.InspectionCase;
import net.ubtuni.resourceserver.inspections.models.Inspector;
import net.ubtuni.resourceserver.locationdata.model.StreetInformation;

public interface InspectionCaseRepository extends JpaRepository<InspectionCase, Long>
{
	List<InspectionCase> findByStreetInformation(StreetInformation streetInformation);

	@Query(value = "SELECT ins from InspectionCase ins where ins.streetInformation in " +
			"(select street.id from StreetInformation street where street.cityZoneInformation in" +
			" (select assignment.zone from InspectorLocationAssignment assignment where assignment.inspector = :inspector))")
	List<InspectionCase> getCasesForZone(Inspector inspector);

	@Query(value = "SELECT ins from InspectionCase ins where ins.status = 'CREATED'  and ins.streetInformation in " +
			"(select street.id from StreetInformation street where street.cityZoneInformation in" +
			" (select assignment.zone from InspectorLocationAssignment assignment where assignment.inspector = :inspector))")
	List<InspectionCase> getCreatedCasesForZone(Inspector inspector);


}
