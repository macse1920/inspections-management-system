package net.ubtuni.resourceserver.inspections.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.resourceserver.inspections.models.InspectionCaseReporter;

public interface InspectionCaseReporterRepository extends JpaRepository<InspectionCaseReporter, Long>
{
	Optional<InspectionCaseReporter> findByIdentifier(String identifier);
}
