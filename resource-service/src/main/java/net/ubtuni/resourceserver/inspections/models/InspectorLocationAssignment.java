package net.ubtuni.resourceserver.inspections.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;

import org.springframework.boot.convert.DataSizeUnit;

import lombok.Data;
import net.ubtuni.resourceserver.locationdata.model.CityZoneInformation;

@Entity
@Data
public class InspectorLocationAssignment
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	@JoinColumn(name = "inspector")
	private Inspector inspector;

	@ManyToOne
	@JoinColumn(name = "zone")
	private CityZoneInformation zone;

	private LocalDateTime assignedAt;

	private boolean isActive;

	@PrePersist
	public void prePersist()
	{
		if (assignedAt == null)
		{
			assignedAt = LocalDateTime.now();
		}
	}
}
