package net.ubtuni.resourceserver.inspections.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.resourceserver.inspections.models.InspectionCategory;
import net.ubtuni.userservice.models.Person;

public interface InspectionCategoryRepository extends JpaRepository<InspectionCategory, Long>
{
	Optional<InspectionCategory> findByPerson(Person person);
}
