package net.ubtuni.resourceserver.inspections.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import inspectors.CreateInspectorRequest;
import inspectors.GetInspectorResponse;
import net.ubtuni.resourceserver.core.BaseController;
import net.ubtuni.resourceserver.inspections.services.InspectionCategoryService;
import net.ubtuni.resourceserver.inspections.services.InspectorService;
import net.ubtuni.resourceserver.utils.Views;

@Controller
@RequestMapping(value = "/inspections/inspectors")
public class InspectorController extends BaseController
{
	@Autowired
	private InspectorService inspectorService;

	@Autowired
	private InspectionCategoryService inspectionCategoryService;


	@GetMapping
	public ModelAndView viewInspectors()
	{
		ModelAndView view = Views.MANAGE_INSPECTORS.getView(userService);
		view.addObject("inspectorToCreate", new CreateInspectorRequest());
		view.addObject("categories", inspectionCategoryService.getCategories());


		return view;
	}

	@GetMapping("/data")
	public @ResponseBody
	List<GetInspectorResponse> getData()
	{
		return inspectorService.getInspectors();
	}

	@PostMapping
	public ModelAndView createInspector(@Valid @ModelAttribute CreateInspectorRequest request)
	{
		inspectorService.createInspector(request);

		ModelAndView view = Views.MANAGE_INSPECTORS.getRedirectView(userService);
		view.addObject("inspectorToCreate", new CreateInspectorRequest());
		view.addObject("categories", inspectionCategoryService.getCategories());

		return view;
	}
}
