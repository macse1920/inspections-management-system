package net.ubtuni.resourceserver.inspections.models;

public enum InspectionCaseAccusedPartyType
{
	INDIVIDUAL, BUSINESS_ENTITY
}
