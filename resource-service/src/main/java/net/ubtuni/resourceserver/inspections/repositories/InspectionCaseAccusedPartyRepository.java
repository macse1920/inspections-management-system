package net.ubtuni.resourceserver.inspections.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import net.ubtuni.resourceserver.inspections.models.InspectionCaseAccusedParty;

public interface InspectionCaseAccusedPartyRepository extends JpaRepository<InspectionCaseAccusedParty, Long>
{
	Optional<InspectionCaseAccusedParty> findByIdentifier(String identifier);
}
