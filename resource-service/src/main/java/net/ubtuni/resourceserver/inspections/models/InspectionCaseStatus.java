package net.ubtuni.resourceserver.inspections.models;

import lombok.Getter;

@Getter
public enum InspectionCaseStatus
{
	CREATED
			{
				@Override
				public String toString()
				{
					return "Created";
				}
			},
	OPEN
			{
				@Override
				public String toString()
				{
					return "Open";
				}
			},
	IN_PROGRESS
			{
				@Override
				public String toString()
				{
					return "In progress";
				}
			},
	ON_HOLD
			{
				@Override
				public String toString()
				{
					return "On Hold";
				}
			},
	CLOSED
			{
				@Override
				public String toString()
				{
					return "Closed";
				}
			}
}
