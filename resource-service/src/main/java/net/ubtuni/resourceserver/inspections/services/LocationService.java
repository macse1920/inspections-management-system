package net.ubtuni.resourceserver.inspections.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ubtuni.resourceserver.locationdata.model.CityInformation;
import net.ubtuni.resourceserver.locationdata.model.CityZoneInformation;
import net.ubtuni.resourceserver.locationdata.model.StreetInformation;
import net.ubtuni.resourceserver.locationdata.repositories.CityInformationRepository;
import net.ubtuni.resourceserver.locationdata.repositories.CityZoneInformationRepository;
import net.ubtuni.resourceserver.locationdata.repositories.StreetInformationRepository;

@Service
public class LocationService
{
	@Autowired
	private CityInformationRepository cityInformationRepository;

	@Autowired
	private CityZoneInformationRepository cityZoneInformationRepository;

	@Autowired
	private StreetInformationRepository streetInformationRepository;

	public List<CityInformation> getCities()
	{
		return cityInformationRepository.findAll();
	}

	public List<CityZoneInformation> getZones(final String city)
	{
		return cityZoneInformationRepository.findAll()
				.stream()
				.filter(inf -> inf.getCity().getName().equals(city))
				.collect(Collectors.toList());
	}

	public List<StreetInformation> getStreets(final String zone){

		return streetInformationRepository.findAll()
				.stream()
				.filter(street -> street.getCityZoneInformation().getName().equals(zone))
				.collect(Collectors.toList());

	}
}
