package net.ubtuni.resourceserver.inspections.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;

import lombok.Data;
import net.ubtuni.resourceserver.locationdata.model.CityZoneInformation;
import net.ubtuni.resourceserver.locationdata.model.StreetInformation;

@Entity
@Data
public class InspectionCase
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String title;

	private String description;

	@Enumerated(EnumType.STRING)
	private InspectionCaseReporterType inspectionCaseReporterType;

	@ManyToOne
	@JoinColumn(name = "reporter")
	private InspectionCaseReporter reporter;

	@Enumerated(EnumType.STRING)
	private InspectionCaseStatus status;

	private String comments;

	private LocalDateTime reportedAt;

	@ManyToOne
	@JoinColumn(name = "accused_party")
	private InspectionCaseAccusedParty inspectionCaseAccusedParty;

	@ManyToOne
	@JoinColumn(name = "zone")
	private StreetInformation streetInformation;

	@PrePersist
	private void prePersist()
	{
		if (reportedAt == null)
		{
			reportedAt = LocalDateTime.now();
		}
	}

	public boolean hasExternalReporter()
	{
		return inspectionCaseReporterType.equals(InspectionCaseReporterType.EXTERNAL) && reporter != null;
	}

}
