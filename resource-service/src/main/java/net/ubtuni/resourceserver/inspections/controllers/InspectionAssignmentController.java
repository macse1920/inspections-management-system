package net.ubtuni.resourceserver.inspections.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import inspectionAssignment.GetAssignmentData;
import inspectionAssignment.UpdateInspectorAssignment;
import net.ubtuni.resourceserver.core.BaseController;
import net.ubtuni.resourceserver.inspections.services.InspectorAssignmentService;
import net.ubtuni.resourceserver.inspections.services.LocationService;
import net.ubtuni.resourceserver.locationdata.model.CityZoneInformation;
import net.ubtuni.resourceserver.utils.Views;

@Controller
@RequestMapping(value = "/inspections/inspectors")
public class InspectionAssignmentController extends BaseController
{
	@Autowired
	private InspectorAssignmentService inspectorAssignmentService;

	@Autowired
	private LocationService locationService;

	@GetMapping(value = "/assignment/view/{id}")
	public ModelAndView view(@PathVariable int id)
	{
		var data = Views.VIEW_INSPECTOR_ASSIGNMENTS.getView(userService);
		data.addObject("id", id);
		data.addObject("cities", locationService.getCities());
		data.addObject("assignmentToUpdate", new UpdateInspectorAssignment());

		return data;
	}

	@PostMapping(value = "/assignment")
	public ModelAndView  updateAssignment(@ModelAttribute UpdateInspectorAssignment assignmentRequest)
	{
		inspectorAssignmentService.updateAssignment(assignmentRequest);

		var data = Views.VIEW_INSPECTOR_ASSIGNMENTS.getRedirectView(userService);
		data.addObject("id", assignmentRequest.getInspectorId());
		data.addObject("cities", locationService.getCities());
		data.addObject("assignmentToUpdate", new UpdateInspectorAssignment());
		return data;

	}

	@GetMapping("/assignment/{id}/data/")
	@ResponseBody
	public List<GetAssignmentData> getAssignmentData(@PathVariable int id)
	{
		return inspectorAssignmentService.getData(id);
	}


}
